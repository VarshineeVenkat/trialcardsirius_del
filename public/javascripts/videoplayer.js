// Dependencies:
// https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js
// https://cdnjs.cloudflare.com/ajax/libs/html5media/1.1.8/html5media.min.js
// https://cdnjs.cloudflare.com/ajax/libs/plyr/3.3.21/plyr.min.js

// Mythium Archive: https://archive.org/details/mythium/
function buildTranscriptDOM(transcript) {
  $("#currentVideo li").remove();
  var str="";
  for (let i = 0; i < transcript.length; i++) {
    $("<li data-start=\"" + transcript[i].startTime + "\" data-end=\"" + transcript[i].endTime + "\"> " + transcript[i].displaytext + " <p class='audioTime'> Found between: " + transcript[i].startTime + "-" + transcript[i].endTime + " seconds. Click to play </p></li>").appendTo("#currentVideo")
    str=str+transcript[i].displaytext ;
  }
  $('#currentVideo li').on('click', function() {
    audio.pause();
    audio.currentTime = $(this).data("start");
    audio.play();
  });
  var json={
    "documents": [{
      "id": "1",
      "text": str
    }]
  }
  $.ajax({
    url: "https://eastasia.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment",
    headers: {
      "Ocp-Apim-Subscription-Key": "7467336cc2a14fdf879bf8c45685870b",
      "Content-Type":"application/json"
    },
    type: "POST",
    data: JSON.stringify(json),
    success: function(data) {
      console.log(data);
      let score=Math.round(data.documents[0].score*100);
      if(score>70){
      $('#sentiment').css({"color":"#4eff6f"});
    } else if (score > 35){
        $('#sentiment').css({"color":"#f7cc61f0"});
    } else{
        $('#sentiment').css({"color":"#8f0a0af0"});
    }
      score = score + "% Positive";
      $('#sentiment').html(score);

    }
  });
}

function buildDOM(response) {
  console.log("buildingDom");
  tracks = []
  audio.pause();

  $(tracks).remove();
  $("#plList li").remove();
  $("#currentVideo li").remove();
  trackCount = response.filenames.length;

  for (var i = 0; i < response.filenames.length; i++) {
    let track = {}
    track.track = i + 1;
    track.name = response.filenames[i];
    track.duration = response[response.filenames[i]].transcript[0].startTime;
    track.file = response.filenames[i];
    track.transcripts = response[response.filenames[i]].transcript;
    tracks.push(track);
  }
  $(tracks).each(function(key, value) {
    var trackNumber = value.track,
      trackName = value.name,
      trackDuration = value.duration;
    if (trackNumber.toString().length === 1) {
      trackNumber = '0' + trackNumber;
    }
    $('#plList').append('<li> \
           <div class="plItem"> \
               <span class="plNum">' + trackNumber + '.</span> \
               <span class="plTitle">' + trackName + '</span> \
               <span class="plLength">' + trackDuration + '</span> \
           </div> \
       </li>');
  })
  $('#plList li').on('click', function() {
    var id = parseInt($(this).index());
    if (id !== index) {
      playTrack(id);
    }
  })

}

//jQuery(function ($) {
//  'use strict'
var supportsAudio = !!document.createElement('audio').canPlayType;
if (supportsAudio) {
  // initialize plyr
  var player = new Plyr('#audio1', {
    controls: [
      'restart',
      'play',
      'progress',
      'current-time',
      'duration',
      'mute',
      'volume'
    ]
  });
  // initialize playlist and controls
  var index = 0,
    isFirstLoaded = false,
    playing = false,
    mediaPath = 'audio/',
    extension = '',
    /*tracks = [{
        "track": 1,
        "name": "All This Is - Joe L.'s Studio",
        "duration": "2:46",
        "file": "JLS_ATI"
    }, {
        "track": 2,
        "name": "The Forsaken - Broadwing Studio (Final Mix)",
        "duration": "8:30",
        "file": "BS_TF"
    }],*/
    tracks = [],
    buildPlaylist = $(tracks).each(function(key, value) {
      var trackNumber = value.track,
        trackName = value.name,
        trackDuration = value.duration;
      if (trackNumber.toString().length === 1) {
        trackNumber = '0' + trackNumber;
      }
      $('#plList').append('<li> \
                    <div class="plItem"> \
                        <span class="plNum">' + trackNumber + '.</span> \
                        <span class="plTitle">' + trackName + '</span> \
                        <span class="plLength">' + trackDuration + '</span> \
                    </div> \
                </li>');
    }),
    trackCount = tracks.length,
    npAction = $('#npAction'),
    npTitle = $('#npTitle'),
    audio = $('#audio1').on('play', function() {

      playing = true;
      npAction.text('Now Playing...');
      if (!isFirstLoaded) {
        loadTrack(index);
        isFirstLoaded = true;
      }

    }).on('pause', function() {
      playing = false;
      npAction.text('Paused...');
    }).on('ended', function() {
      npAction.text('Paused...');
      if ((index + 1) < trackCount) {
        index++;
        loadTrack(index);
        audio.play();
      } else {
        audio.pause();
        index = 0;
        loadTrack(index);
      }
    }).get(0),
    btnPrev = $('#btnPrev').on('click', function() {
      if ((index - 1) > -1) {
        index--;
        loadTrack(index);
        if (playing) {
          audio.play();
        }
      } else {
        audio.pause();
        index = 0;
        loadTrack(index);
      }
    }),
    btnNext = $('#btnNext').on('click', function() {
      if ((index + 1) < trackCount) {
        index++;
        loadTrack(index);
        if (playing) {
          audio.play();
        }
      } else {
        audio.pause();
        index = 0;
        loadTrack(index);
      }
    }),
    li = $('#plList li').on('click', function() {
      var id = parseInt($(this).index());
      if (id !== index) {
        playTrack(id);
      }
    }),
    loadTrack = function(id) {
      $('.plSel').removeClass('plSel');
      $('#plList li:eq(' + id + ')').addClass('plSel');
      npTitle.text(tracks[id].name);
      index = id;
      // removing media path to point to remote location
      //audio.src = mediaPath + tracks[id].file;
      audio.src = tracks[id].file;
      let track = tracks[index];
      let transcript = track.transcripts
      buildTranscriptDOM(transcript)


      audio.currentTime = transcript[0].startTime;

    },
    playTrack = function(id) {
      loadTrack(id);
      /*let track=tracks[index];
      let transcript =track.transcripts
      for(let i=0;i<transcript.length;i++){
        buildBotMsg("The following transcript was found between "+transcript.startTime+" and "+ transcript.endTime);
        buildBotMsg(transcript.displaytext)
      }
      audio.currentTime=transcript[0].startTime;*/
      audio.play();
    };
  extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
  //loadTrack(index);
} else {
  // boo hoo
  $('.column').addClass('hidden');
  var noSupport = $('#audio1').text();
  $('.container').append('<p class="no-support">' + noSupport + '</p>');
}
//});
