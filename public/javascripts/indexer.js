console.log('indexer is loader');
var url_string = window.location.href;
var url = new URL(url_string);
var accountID = url.searchParams.get("accountID");//'00000000-0000-0000-0000-000000000000';
var videoID = url.searchParams.get("videoID");//'b073f41919';
var subscriptionKey = '27df2361054641d9aa35673ae6adf10a';
var proxy = 'https://cors-anywhere.herokuapp.com/';
var endurl = proxy + "https://api.videoindexer.ai/auth/trial/Accounts/" + accountID +"/Videos/" + videoID + "/AccessToken";
var indexURL = "https://api.videoindexer.ai/trial/Accounts/" + accountID + "/Videos/" + videoID + "/Index";
var indexVideoInsertURL = 'http://localhost:8999/indexVideo';
var videoData = [];

$.ajax({
		url : endurl, 
		headers : {
			'Ocp-Apim-Subscription-Key' : subscriptionKey
		},
		success : function(accessToken){
			$('#status').html('AccessToken received... Getting Video Index data...');
        	getVideoIndex(accessToken);
        }
});
function getVideoIndex(accessToken){
	console.log(accessToken);
	$.ajax({
			url : indexURL, 
    		data : {
				'accessToken' : accessToken,
	 				'language' : 'English'
			},
			success : function(response){
				$('#status').html('Video Index data received... Inserting Video Index into mLabs...');
				console.log(response);
				var transcript = '';
				for(var i =0 ; i < response.videos[0].insights.transcript.length; i++){
					transcript += response.videos[0].insights.transcript[i].text;
					/*if(transcript[transcript.length - 1] != '.')
            			transcript += '.';
          			transcript += ' ';*/
					videoData.push({
						'text' : response.videos[0].insights.transcript[i].text,
						'start' : response.videos[0].insights.transcript[i].instances[0].start,
						'end' : response.videos[0].insights.transcript[i].instances[0].end
					});
				}
				callInsertVideoIndex(response.name, transcript);
			}
    });
}
function callInsertVideoIndex(name, transcript){
	transcript = transcript.replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');
	$.ajax({
		url : indexVideoInsertURL, 
		type : 'post',
		dataType : 'json',
		data : {
			'name' : name,
			'transcript' : transcript
		},
		success : function(response){
			alert('succ');
			$('#status').html('Insertion completed successfully...');
        	console.log(response);
        }
	});
}