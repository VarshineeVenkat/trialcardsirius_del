var $messages = $('.messages-content'),
  d, h, m,
  i = 0;
var videoData = [];
var HOST = location.origin.replace(/^http/, 'ws')
var ws = new WebSocket(HOST);
var transcript = '';
$(window).load(function() {
  setTimeout(function(){
        $('body').addClass('loaded');
    }, 1000);

  $messages.mCustomScrollbar();
  setTimeout(function() {
    buildBotMsg("Hey there, I am Avid.AI");
  }, 100);
  ws.onopen = function() {

    console.log('websocket is connected ...');
  }
  ws.onmessage = function(ev) {
    console.log(ev);
    let result=JSON.parse(ev.data);
    buildDOM(result);
    /*console.log(JSON.parse(ev.data).answer.best_span_str);
    var pos = JSON.parse(ev.data).answer.best_span[0];
    console.log(transcript + ' '+ pos);
    getContainedSentence(transcript, pos)
    let data = JSON.parse(ev.data)
    buildBotMsg(data.answer.best_span_str);*/
    //let response = $('<li class="bot"><div>' + data.answer.best_span_str + '</div></li>');
    //$('.chat-history ul').append(response);
  }
  //getVideoData();
});


function updateScrollbar() {
  $messages.mCustomScrollbar("update").mCustomScrollbar('scrollTo', 'bottom', {
    scrollInertia: 10,
    timeout: 0
  });
}

function setDate() {
  d = new Date()
  if (m != d.getMinutes()) {
    m = d.getMinutes();
    $('<div class="timestamp">' + d.getHours() + ':' + m + '</div>').appendTo($('.message:last'));
    $('<div class="checkmark-sent-delivered">&check;</div>').appendTo($('.message:last'));
    $('<div class="checkmark-read">&check;</div>').appendTo($('.message:last'));
  }
}

function insertMessage() {
  msg = $('.message-input').val();
  if ($.trim(msg) == '') {
    return false;
  }
  $('<div class="message message-personal">' + msg + '</div>').appendTo($('.mCSB_container')).addClass('new');
  setDate();
  $('.message-input').val(null);
  updateScrollbar();
  ws.send(msg);
  /*setTimeout(function() {
    buildBotMsg("blah");
  }, 1000 + (Math.random() * 20) * 100);*/
}

$('.message-submit').click(function() {
  insertMessage();
});


$(window).on('keydown', function(e) {
  if (e.which == 13) {
    insertMessage();
    return false;
  }
})



function buildBotMsg(msg) {
  if ($('.message-input').val() != '') {
    return false;
  }
  $('<div class="message loading new"><figure class="avatar"><img src="https://upload.wikimedia.org/wikipedia/commons/3/38/Robot-clip-art-book-covers-feJCV3-clipart.png" /></figure><span></span></div>').appendTo($('.mCSB_container'));
  updateScrollbar();

  setTimeout(function() {
    $('.message.loading').remove();
    $('<div class="message new"><figure class="avatar"><img src="https://upload.wikimedia.org/wikipedia/commons/3/38/Robot-clip-art-book-covers-feJCV3-clipart.png" /></figure>' + msg + '</div>').appendTo($('.mCSB_container')).addClass('new');
    setDate();
    updateScrollbar();
  }, 1000 + (Math.random() * 20) * 100);

}

$('.button').click(function() {
  $('.menu .items span').toggleClass('active');
  $('.menu .button').toggleClass('active');
});

function getVideoData(){
var url_string = window.location.href;
var url = new URL(url_string);
var accountID = url.searchParams.get("accountID");//'00000000-0000-0000-0000-000000000000';
var videoID = url.searchParams.get("videoID");//'b073f41919';

var subscriptionKey = '27df2361054641d9aa35673ae6adf10a';
var proxy = 'https://cors-anywhere.herokuapp.com/';
var endurl = proxy + "https://api.videoindexer.ai/auth/trial/Accounts/" + accountID +"/Videos/" + videoID + "/AccessToken";
var indexURL = "https://api.videoindexer.ai/trial/Accounts/" + accountID + "/Videos/" + videoID + "/Index";
var indexVideoInsertURL = 'http://localhost:8999/indexVideo';


$.ajax({
    url : endurl,
    headers : {
      'Ocp-Apim-Subscription-Key' : subscriptionKey
    },
    success : function(accessToken){
          getVideoIndex(accessToken);
        }
});
function getVideoIndex(accessToken){
  console.log(accessToken);
  $.ajax({
      url : indexURL,
        data : {
        'accessToken' : accessToken,
          'language' : 'English'
      },
      success : function(response){

        console.log(response);
        transcript = '';
        for(var i =0 ; i < response.videos[0].insights.transcript.length; i++){
          transcript += response.videos[0].insights.transcript[i].text;
          /*if(transcript[transcript.length - 1] != '.')
            transcript += '.';
          transcript += ' ';*/
          videoData.push({
            'text' : response.videos[0].insights.transcript[i].text,
            'start' : response.videos[0].insights.transcript[i].instances[0].start,
            'end' : response.videos[0].insights.transcript[i].instances[0].end
          });
        }
        transcript = transcript.replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');
      }
    });
}
}


function getContainedSentence(transcript, pos){
  var str=transcript;
  var mStr=[];
  var position = pos-1;


  var split=str.split(" ");
  var inc=0;
  for(var i=0;i<split.length;i++){
        var temp=split[i];

        if(temp.includes(".")){

          for(var j=0;j<temp.length;j++){

              if(temp[j]==="."){
                  if(temp.substring(0,j)!=="" && temp.substring(0,j)!==" "){

                      mStr[inc]=temp.substring(0,j);
                      inc=inc+1;

                  }

                  if(j+1<=temp.length){
                      if(temp.substring(j,j+1)!== "" && temp.substring(j,j+1)!== " "){

                           mStr[inc]=(temp.substring(j,j+1)).toString();
                           inc=inc+1;


                      }
                      if(temp.substring(j+1,temp.legth)!== "" && temp.substring(j+1,temp.legth)!== " "){

                           mStr[inc]=temp.substring(j+1,temp.length);
                           inc=inc+1;

                      }
                  }


              }
          }


     }
      else{
            mStr[inc]=temp;
            inc=inc+1;
      }
  }
        var start,end=0;
        for(var i=position-1;i>=0;i--){
                   if(mStr[i].includes(".")){
                       start=i+1;
                       break;
                   }

         }
          for(var j=position+1;i<mStr.length;j++){
                   if(mStr[j].includes(".")){
                       end=j-1;
                       break;
                   }
          }
        var ans="";
         for(var i=start;i<=end;i++){
             ans=ans+mStr[i]+" ";
         }
        console.log('****'+ans);
        getTimeFromSentence(ans.trim());

}
function getTimeFromSentence(sentence){
  var timeStamp = 0;
  for(var i=0;i<videoData.length;i++){
    cVideTranscript = videoData[i].text.replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');
    if(cVideTranscript.indexOf(sentence)!=-1 && videoData[i].start){
      var timeStampStr = videoData[i].start.split(':');
      console.log(videoData[i]);
      timeStamp = parseInt(timeStampStr[0])*3600 + parseInt(timeStampStr[1])*60 + parseInt(timeStampStr[2])
      break;
    }
  }
  var media = document.getElementById('player_html5_api');
  media.currentTime = timeStamp - 2;
  media.play();
}
