var q = require('q');
const sleep = require('util').promisify(setTimeout)
var fileName = "";
const dbutils = require('./dbutil')
var constants = require('./constants')
var results = []
var lowercaseKeys = require('lowercase-keys');
var https = require("https");
var querystring = require('querystring');
var optionsPath = "/api/as/v1/engines/siriusavid/";
var allResults = "";
var path = require('path');
const fs = require('fs');

var options = {
  host: "host-h9yijb.api.swiftype.com",
  path: optionsPath,
  method: "POST",
  headers: {
    "Content-Type": "application/json",
    "Authorization": "Bearer private-6pw81xvrp8naex5a63bc3hvw",
    'User-Agent': 'Chrome'
  }
};


module.exports = {

  myfun: function(recognizer,tempFileName, fileName) {



    console.log("insider myfun")

    console.log("fileName" + fileName);

    recognizer.currentResponse = {};
    recognizer
      .start()
      .then(_ => {
        console.log("hii");
        recognizer.on('recognition', (e) => {
          if (e.RecognitionStatus === 'Success') {

            console.log("success");
            console.log(e)
            if (recognizer.currentResponse.hasOwnProperty(fileName)) {
              console.log("pushing into" + fileName)
              recognizer.currentResponse[fileName].result.push(e);
              e.filename = fileName;
              e = lowercaseKeys(e);
              allResults = allResults + e.displaytext + "\n";
              results.push(e)

              dbutils.insertRecord(results, constants.AUDIO_TRANSCRIPT);
              this.pushToESService(results);
              results = [];

            } else {
              recognizer.currentResponse[fileName] = {}
              recognizer.currentResponse[fileName].result = [];
              recognizer.currentResponse[fileName].result.push(e);
              e.filename = fileName;
              allResults = allResults + e.displaytext + "\n";
              e = lowercaseKeys(e);
              results.push(e)

              dbutils.insertRecord(results, constants.AUDIO_TRANSCRIPT);
              this.pushToESService(results);
              results = [];

            }
            // if (results.length > 80) {
             
              // dbutils.insertRecord(results, constants.AUDIO_TRANSCRIPT);
              // this.pushToESService(results);
              // results = [];
            // }
          }
        });

        console.log("here");
        if (fileName.includes(".wav") || fileName.includes(".mp3")) {
            recognizer.sendFile(tempFileName)
              .then(_ => {
                console.log('file sent.');
                allResults = allResults + "fileName:" + fileName + "\n";
              })
              .catch(console.log("What is error "));
        } 
 
      })
      .catch(console.error);
  
  },
  writeToFile: function(allTranscripts) {
    var fileDirectoryPath = path.join(__dirname, 'output');
    fs.writeFile(fileDirectoryPath + "/test.txt", allTranscripts, function(err) {
      if (err) {
        return console.log(err);
      }

      console.log("The file was saved!");
    });
  },
  pushToESService: function(transcriptJSON) {
    for (var j = 0; j < transcriptJSON.length; j++) {
      delete transcriptJSON[j]['_id'];
    }
    //
    console.log('inserting**' + JSON.stringify(transcriptJSON));

    options.path = optionsPath + 'documents';
    console.log(options);
    var req = https.request(options, function(res) {
      var responseString = "";

      res.on("data", function(data) {
        responseString += data;
        console.log("res data" + data);
        // save all the data from response
      });
      res.on('error', function(err) {
        console.log("--------------");
        console.log(options);
        console.log("res-err**");
        console.log(err);

      });

    });
    // req error
    req.on('error', function(err) {
      console.log("--------------");
      console.log(options);
      console.log("err**");
      console.log(err);
    });
    req.write(JSON.stringify(transcriptJSON));
    req.end();
    //
  },
  searchFromESService: function(searchString, ws) {
    console.log('searching for ' + searchString);
    //
    options.path = optionsPath + 'search';
    console.log(options);
    var req = https.request(options, function(res) {
      var responseString = "";
      var response = {}
      response.filenames = [];
      var transcripts = [];
      res.on("data", function(data) {
        responseString += data;
        console.log("body>>" + data);

      });
      res.on('end', function(err, data1) {
        let data = JSON.parse(responseString);
        if (data.hasOwnProperty("results")) {
          for (var i = 0; i < data.results.length; i++) {
            console.log("iterating over results");
            let res = data.results[i];
            let tempFileName = res.filename.raw;
            let filename;
            if (tempFileName.includes("audio")) {
              filename = tempFileName.substring(tempFileName.indexOf("audio/"), tempFileName.length);
              filename = filename.replace("audio/", "");
              
              //WAV REPLACE
              filename = filename.replace(".wav", "");
              console.log("iterating over results " + filename);
            } else {
              filename = tempFileName.substring(tempFileName.indexOf("newRecordings/"), tempFileName.length);
              filename = filename.replace("newRecordings/", "");
              console.log("iterating over results " + filename);
            }
            if (!transcripts.includes(res.displaytext.raw)) {

              if (!response.filenames.includes(filename)) {
                response.filenames.push(filename);
              }
              transcripts.push(res.displaytext.raw);
              if (!response.hasOwnProperty(filename)) {
                response[filename] = {}
                response[filename].transcript = [];
                let transcript = {}
                transcript.displaytext = res.displaytext.raw;
                transcript.startTime = Math.round(res.offset.raw / 10000000);
                transcript.endTime = Math.round(transcript.startTime + (res.duration.raw / 10000000));
                response[filename].transcript.push(transcript);
              } else {
                let transcript = {}
                transcript.displaytext = res.displaytext.raw;
                transcript.startTime = Math.round(res.offset.raw / 10000000);
                transcript.endTime = Math.round(transcript.startTime + (res.duration.raw / 10000000));
                response[filename].transcript.push(transcript);
              }
              console.log("iterating over results " + JSON.stringify(response));

            }
          }
          console.log("iterating over results " + JSON.stringify(response));

          ws.send(JSON.stringify(response));
        } else {
          console.log("no result");
        }
      });
      res.on('error', function(err) {
        console.log("res-err**");
        console.log(err);
      });

    });
    var reqBody = {
      "query": searchString
    };
    var postData = querystring.stringify(reqBody);
    // req error
    req.on('error', function(err) {
      console.log("err**");
      console.log(err);
    });
    req.write(JSON.stringify(reqBody));
    req.end();
    //
  },
  multiSearch: function(entries, ws) {
    options.path = optionsPath + 'multi_search';
    var queries = [];
    for (var k = 0; k < entries.length; k++) {
      queries.push({
        "query": entries[k]
      });
    }
    var req = https.request(options, function(res) {
      var responseString = "";
      var response = {}
      response.filenames = [];
      var transcripts = [];
      res.on("data", function(data) {
        responseString += data;
        console.log("body>>" + data);


      });
      res.on('end', function(err, data1) {
        let datas = JSON.parse(responseString);

        for (let j = 0; j < datas.length; j++) {
          console.log(datas[j].results)
          let data = datas[j];
          if (data.hasOwnProperty("results")) {
            for (var i = 0; i < data.results.length; i++) {

              console.log("iterating over results");
              let res = data.results[i];
              if (res._meta.score >= 1) {
                let tempFileName = res.filename.raw;
                let filename;
                if (tempFileName.includes("audio")) {
                  filename = tempFileName.substring(tempFileName.indexOf("audio/"), tempFileName.length);
                  
                  //wav file replace
                  filename = filename.replace(".wav", "");
                  
                  
                  filename = filename.replace("audio/", "");
                  console.log("iterating over results " + filename);
                } else {
                  filename = tempFileName.substring(tempFileName.indexOf("newRecordings/"), tempFileName.length);
                  filename = filename.replace("newRecordings/", "");
                  console.log("iterating over results " + filename);
                }
                if (!transcripts.includes(res.displaytext.raw)) {

                  if (!response.filenames.includes(filename)) {
                    response.filenames.push(filename);
                  }
                  transcripts.push(res.displaytext.raw);
                  if (!response.hasOwnProperty(filename)) {
                    response[filename] = {}
                    response[filename].transcript = [];
                    let transcript = {}
                    transcript.displaytext = res.displaytext.raw;
                    transcript.startTime = Math.round(res.offset.raw / 10000000);
                    transcript.endTime = Math.round(transcript.startTime + (res.duration.raw / 10000000));
                    response[filename].transcript.push(transcript);
                  } else {
                    let transcript = {}
                    transcript.displaytext = res.displaytext.raw;
                    transcript.startTime = Math.round(res.offset.raw / 10000000);
                    transcript.endTime = Math.round(transcript.startTime + (res.duration.raw / 10000000));
                    response[filename].transcript.push(transcript);
                  }
                  console.log("iterating over results " + JSON.stringify(response));

                }


              }
            }
          } else {
            console.log("no result");
          }
        }
        console.log("iterating over results " + JSON.stringify(response));

        ws.send(JSON.stringify(response));
      });
      res.on('error', function(err) {
        console.log("res-err**");
        console.log(err);
      });
    });
    var reqBody = {
      "queries": queries
    };
    console.log(reqBody);
    req.write(JSON.stringify(reqBody));
    req.end();
  }
}
