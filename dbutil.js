var MongoClient = require('mongodb').MongoClient;
var database;
var constants= require('./constants')
var q = require('q');

// Use connect method to connect to the Server
// Init method
MongoClient.connect(constants.MONGO_URI, function(err, db) {
    if (err) {
        console.error('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to: ' + constants.MONGO_URI);
        // making use of connection pool
        database = db.db(constants.MONGO_DB_NAME);;
    }
});
module.exports = {
    insertRecord: function(record, table) {
        console.log('Requested to insert: ' + JSON.stringify(record) + " in collection - " + table);
        // Get the documents collection
        var collection = database.collection(table);
        var insertResult = {
            error: false
        }
        var deferred = q.defer();

        // Insert user record into the collection as new document
        collection.insert(record, function(err, result) {
            if (err) {
                console.error(err);
                insertResult.error = true;
                deferred.reject(insertResult);
            } else {
                console.log('Inserted document into the "'+ table +'" collection. The documents inserted with "_id" are:', JSON.stringify(result));
                insertResult.error = false;
                insertResult.message = result;
                deferred.resolve(insertResult);
            }
        });
        console.log('Exiting insertRecord method');
        return deferred.promise;
    },
    getAdverseEffects:function(){

      var deferred = q.defer();
      let table ="adverseEffects";
      console.log("Requested all the records from collection: " + table);

      // Get the documents collection
      var collection = database.collection(table);
      collection.find().toArray(function(err, result) {
          var response = {};
          if (err) {
              console.log(err);
              response.error = true;
              response.errorObject = err;
              deferred.reject(response);
          } else if (result.length) {
              // console.log('Found:', result);
              response.error = false;
              response.record = result;
              deferred.resolve(response);
          } else {
              response.error = false;
              response.record = [];
              console.log('No document(s) found with defined "find" criteria!');
              deferred.resolve(response);
          }
      });

      return deferred.promise;
    },

    getAllTranscripts:function () {
      var deferred = q.defer();
      let table ="audio_transcript";
      console.log("Requested all the records from collection: " + table);

      // Get the documents collection
      var collection = database.collection(table);
      collection.find().sort({ "filename": 1,"offset":1 }).toArray(function(err, result) {
          var response = {};
          if (err) {
              console.log(err);
              response.error = true;
              response.errorObject = err;
              deferred.reject(response);
          } else if (result.length) {
              console.log('Found:', result);
              response.error = false;
              response.record = result;
              deferred.resolve(response);
          } else {
              response.error = false;
              response.record = [];
              console.log('No document(s) found with defined "find" criteria!');
              deferred.resolve(response);
          }
      });

      return deferred.promise;
    }

}
