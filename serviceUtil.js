var constants = require('./constants')
const request = require('request');

module.exports = {
  callBiDaf: function(message, ws) {
    request(constants.BIDAF_URL + constants.PORT + constants.SERVICE + constants.VIDEO_KEY_VALUE + constants.QN + message, {
      json: true
    }, (err, res, body) => {
      if (err) {
        return console.log(err);
      }
      var result={"answer":body,"hasVideoTime":false}
      ws.send(JSON.stringify(result));
    });
  }
}
