var query="https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/64f853c7-e690-4b4b-94e4-226183e01bdc?subscription-key=0a428306683448389df6e7c92be29715&timezoneOffset=-360&q=";
var ssConsumer = require('./speechServiceConsumer');
const dbutils = require('./dbutil')

var request=require('request');
module.exports={

identify: function(msg,ws){
  request(query+msg, function (error, response, body) {
  console.log('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  console.log('body: +++', body); // Print the HTML for the Google homepage.
  let entity=msg;
  let adE=false;
  body=JSON.parse(body);
  console.log(body.hasOwnProperty("topScoringIntent"))
  if(body.hasOwnProperty("topScoringIntent")){
    console.log("body has adverseEffect")
    if(body.topScoringIntent.intent=="adverseEffect"){
      adE=true;
      console.log("adverseEffect")
      dbutils.getAdverseEffects().then(data=>{
        if(!error){
        console.log(data.record[0].effects);
        var effects=data.record[0].effects;
        
        //disabling luis for multisearch for demo
        //ssConsumer.multiSearch(effects,ws);
        }
      })
    }
  }
  console.log(!adE )
  if(!adE && body.hasOwnProperty("entities") && body.entities.length>0){
    entity=body.entities[0].entity;
    console.log("body has entity")

  }
  //if(!adE) 
  
  //enabling all services for demo
  ssConsumer.searchFromESService(entity,ws);

})
}
}
