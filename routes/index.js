var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var bodyParser = require('body-parser');
var dbutils = require('../dbutil');
var ssConsumer = require('../speechServiceConsumer');

const toWav = require('audiobuffer-to-wav');
const AudioContext = require('web-audio-api').AudioContext;


/* GET home page. */
router.get('/', function (req, res, next) {
  res.redirect("/index.html");
});


router.post('/uploadSwift', function (req, res, next) {


  var fileName = req.body.fileName;
  console.log("Inside Function", fileName);

  var ssConsumer = require('../speechServiceConsumer');

  const speechService = require('ms-bing-speech-service');

  const options = {
    subscriptionKey: '120680ab04194c0cb5c6bc7697213fda',
    serviceUrl: 'wss://westus.stt.speech.microsoft.com/speech/recognition/conversation/cognitiveservices/v1?language=en-US&Transfer-Encoding=chunked&Ocp-Apim-Subscription-Key=120680ab04194c0cb5c6bc7697213fda',
    issueTokenUrl: 'https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken'
  };
  const recognizer = new speechService(options);


  console.log("hello poc")


  var fullFileName = "https://trialaiblobfunc8255.blob.core.windows.net/audiofilemp3/"+fileName

  var request = require('request').defaults({ encoding: null });
  request.get(fullFileName, function (err, res, body) {


    var fs = require('fs');
    const audioContext = new AudioContext;

    audioContext.decodeAudioData(body, buffer => {
      let wav = toWav(buffer);

      var chunk = new Uint8Array(wav);

      var tempFile = fs.createWriteStream("./temp1.wav");
      tempFile.on('open', function (fd) {
        tempFile.write(chunk);
        tempFile.end();
        ssConsumer.myfun(recognizer, "./temp1.wav", fullFileName);


      });

    });


  });

  res.send('success');


});


router.get('/getAllTranscripts', function (req, res, next) {
  dbutils.getAllTranscripts().then(data => {

    console.log("getAllTranscripts------------");
    console.log(data);
    let filename = "";
    let previousTranscript = "";
    let allTranscripts = "";
    var result = [];
    for (var i = 0; i < data.record.length; i++) {
      console.log(i);
      if (filename == data.record[i].filename) {
        console.log("filename equals");
        if (data.record[i].displaytext == previousTranscript) {
          continue;
        } else {
          result.push(data.record[i]);
          allTranscripts = allTranscripts + data.record[i].displaytext + "\n";
          previousTranscript = data.record[i].displaytext;
        }
      } else {
        console.log("filename not equals");

        allTranscripts = allTranscripts + "-----------------------\n";
        filename = data.record[i].filename;
        allTranscripts = allTranscripts + "\n Filename: " + filename + ":" + "\n";
        if (data.record[i].displaytext == previousTranscript) {
          continue;
        } else {
          console.log("in else not equals");

          allTranscripts = allTranscripts + data.record[i].displaytext + "\n";
          previousTranscript = data.record[i].displaytext;
          console.log("in else not equals");

          result.push(data.record[i]);
          console.log("pushed", result);

        }
      }
      if (result.length > 80) {
        ssConsumer.pushToESService(result);
        result = [];
      }
      console.log(filename)
      console.log(allTranscripts)


    }
    ssConsumer.writeToFile(allTranscripts);
    res.send(allTranscripts);

  })
});


router.get('/hello', function (req, res, next) {

  console.log(req.body.abc)
  res.send(req.body.abc);
});



router.get('/indexer', function (req, res, next) {

  res.redirect("/indexer.html");
});
router.post('/indexVideo', function (req, res, next) {
  mlabInsert(req.body.name, req.body.transcript);
  res.send('success');
});
router.get('/mlabGet', function (req, res, next) {
  var passage = mlabFind(req.params.videoName);
  res.send(passage);
});
function mlabInsert(name, transcript) {
  console.log('inserting into mlab');

  MongoClient.connect("mongodb://user:user123@ds229732.mlab.com:29732/videoai", function (err, db) {
    if (err) throw err;
    var dbo = db.db("videoai");
    var myobj = { video: name, passage: transcript };
    dbo.collection("transcripts").insertOne(myobj, function (err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    });
  });
}
function mlabFind(videoName) {
  var passage = '';
  MongoClient.connect("mongodb://user:user123@ds229732.mlab.com:29732/videoai", function (err, db) {
    var dbo = db.db("videoai");
    dbo.collection("transcripts").findOne({ video: videoName }, function (err, document) {
      console.log(document.passage);
      passage = document.passage;
    });
  });
  return passage;
}
module.exports = router;
