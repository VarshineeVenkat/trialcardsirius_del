var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http')
var index = require('./routes/index');
var app = express();
var WebSocketServer = require("ws").Server


var ws = require('./ws');
var ssConsumer = require('./speechServiceConsumer');
const fs = require('fs')


const speechService = require('ms-bing-speech-service');


const options = {
  subscriptionKey: '120680ab04194c0cb5c6bc7697213fda',
  serviceUrl: 'wss://westus.stt.speech.microsoft.com/speech/recognition/conversation/cognitiveservices/v1?language=en-US&Transfer-Encoding=chunked&Ocp-Apim-Subscription-Key=120680ab04194c0cb5c6bc7697213fda',
  issueTokenUrl: 'https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken'
};


const recognizer = new speechService(options);

const directoryPath = path.join(__dirname, 'public/audio');
const Lame = require("node-lame").Lame;

var path = require('path');


const port = process.env.PORT || 8000;


fs.watch(directoryPath, { persistent: true }, function (event, fileName) {
   console.log("Event: " + event);
   console.log(fileName + "\n");
   var arrFiles = [];
   arrFiles.push(fileName);
   console.log(arrFiles);
   ssConsumer.myfun(recognizer,directoryPath+"/", arrFiles,0);
});
/*fs.readdir(directoryPath, {
  withFileTypes: true
}, async function(err, files) {
  //handling error
  if (err) {
    return console.log('Unable to scan directory: ' + err);
  }
  //ssConsumer.myfun(recognizer,directoryPath+"/",files,0);
  /*for (var i = 0; i < files.length; i++) {
    if (files[i].name.includes(".mp3")) {
      console.log(directoryPath+"/"+files[i].name);
      var decoder = new Lame({
        "output": directoryPath+"/"+files[i].name + ".wav"
      }).setFile(directoryPath+"/"+files[i].name);

      decoder.decode()
        .then(() => {
          // Decoding finished
          console.log("done");
        })
        .catch((error) => {
          // Something went wrong
          console.log(error);
        });
    }
//  }
});*/


//const promises = files.map(async rec =>{ if(rec.includes(".wav")) await ssConsumer.myfun(recognizer,directoryPath+"/"+rec)})
//var result=await Promise.all(promises);
/*const promises=[]
for(const rec of files){
  if(rec.includes(".wav"))
    promises.push(ssConsumer.myfun(recognizer,directoryPath+"/"+rec));*/

//}
/*Promise.all(promises).then(_=>
{

console.log("gotcha");
console.log(recognizer.currentResponse)})
//listing all files using forEach
/*files.forEach(function (file) {
    // Do whatever you want to do with the file
    console.log(file);

      ssConsumer.myfun(recognizer,directoryPath+"/"+file);
});*/
//});

//ssConsumer.myfun(recognizer,"1.wav");

/*
let audioStream = fs.createReadStream("speech.wav");

const speechService = require('ms-bing-speech-service');

const options = {
  language: 'en-US',
  subscriptionKey: 'ee7a6ae417844734b6baa0a8522689e7'
};

const recognizer = new speechService(options);

//const audioStream = fs.createReadableStream('speech.wav');

let audioStream = fs.createReadStream("speech.wav");

const speechService = require('ms-bing-speech-service');

const options = {
  language: 'en-US',
  subscriptionKey: 'ee7a6ae417844734b6baa0a8522689e7'
};

const recognizer = new speechService(options);

//const audioStream = fs.createReadableStream('speech.wav');
*/
/*
async function myfun() {

  const options = {
    language: 'en-US',
    subscriptionKey: 'ee7a6ae417844734b6baa0a8522689e7'
  };

  const recognizer = new speechService(options);
  await recognizer.start();
  recognizer.currentFile="speech.wav";
  recognizer.currentResponse={}
  recognizer.on('recognition', (e) => {
    if (e.RecognitionStatus === 'Success') {
      //console.log("success");
    //  console.log(e);
    if(recognizer.currentResponse.hasOwnProperty(recognizer.currentFile)){
      recognizer.currentResponse[recognizer.currentFile].result.push(e)
    } else {
        recognizer.currentResponse[recognizer.currentFile]={}
        recognizer.currentResponse[recognizer.currentFile].result=[];
          recognizer.currentResponse[recognizer.currentFile].result.push(e);
    }
  }
  });
  recognizer.on('turn.end', async (e) => {

  console.log('recognizer is finished.'+ JSON.stringify(recognizer.currentResponse));
    await recognizer.stop();
    console.log('recognizer is stopped.');
  });

  await recognizer.sendFile(recognizer.currentFile);
  console.log('file sent.');

}

myfun();



/*
recognizer
  .start()
  .then(_ => {
    recognizer.on('recognition', (e) => {
      if (e.RecognitionStatus === 'Success') console.log(e);
    });
    recognizer.on('turn.end',(e)=>{
      console.log(e)
    })
    recognizer.sendFile('speech.wav')
      .then(_ => console.log('file sent.'))
      .catch(console.error);
  })
  .catch(console.error);
/*
// Bing Speech Key (https://www.microsoft.com/cognitive-services/en-us/subscriptions)
let subscriptionKey = 'ee7a6ae417844734b6baa0a8522689e7';
let client = new BingSpeechClient(subscriptionKey);

          client.recognizeStream(audioStream).then(function(response)
          {

            console.log("response is ",JSON.stringify(response));
          }).catch(function(error)
          {
            console.log("error occured is ",error);
          });

*/
//start our server



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var server = http.createServer(app)
server.listen(port)



var constants = require('./constants');
var serviceUtil = require('./serviceUtil');
var luis= require('./luis');

//

var wss = new WebSocketServer({server: server})
console.log("websocket server created")


wss.on('connection', function(ws) {
  ws.on('message', function(message) {
      luis.identify(message,ws);
      //ssConsumer.searchFromESService(message,ws);
    //serviceUtil.callBiDaf(message, ws);
  })

  ws.on("close", function() {
    console.log("websocket connection close")
   // clearInterval(id)
  })

})




process.on('uncaughtException', function(err) {
  console.log('Caught exception: ', err);
});

module.exports = app;
